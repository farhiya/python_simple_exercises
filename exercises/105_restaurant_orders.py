# Covid Menu

# User Stories #1
# AS a person having dinner, I want to be able to see the menu in a nice way, so that I can order my meal correttly.

# User Stories #2
# AS a person having dinner, I want to be able to order 3 items, and have my responses added to a list as to be used later.

# User Stories #3
# As a person having dinner, I want the system to be able to read out my selections, so I can see what I ordered.

# starter code
la_carte = ['steak and potatoes', 'salmon', 'bbq ribs', 'chicken wings', 'chips']
count = 1

print("la carte:")

for item in la_carte:
    print(count,":",item)
    count += 1


food_order = []
