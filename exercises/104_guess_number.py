import random
# Gueesing the magic number game!
# equate objects and generate booleans

# As a client, I want to be able to guess a number and know if i got it correct or not, so that I can know if I won or not.


#  Define/assign number to a variable called magic_number

# everytime program is runs, random integer between 0 and 100 is selected as magic_number by the computer
magic_number= random.randint(0, 100)

# Ask the user for a guess

guess = int(input("Guess the magic number (It's between 0 and 100): "))

# checks just once
if guess == magic_number:
    print("Correct! You guessed the magic number bro!")
else:
    print("Better luck next time!")

# keep asking user till it gets it right
while magic_number != guess:
    print(magic_number)
    print("Oppsy! Guess again: ")
    guess = int(input("Guess the magic number: "))
else:
    print("Correct! You guessed the magic number!")


# Check if this input matches a magic_number



# Give user feedback regarding their guess



