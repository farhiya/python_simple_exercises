# Lists - LET's use some lists!
# Define a list with amazing things inside!
    # For example, best places to eat or things you would do if you won the lottery? 
    # It must have 5 items
    # Complete the sentence:
    # Lists are organized using: __index_____????? 


## Define a list with 5 amazing things inside!

students = ['james', 'kofi', 'amalia', 'shannon', 'arslan']

# Print the lists and check the data type of the list?

print(students)

# Print the list's first object

print(students[0])


# Print the list's second object

print(students[1])

# Check the type of the list's first and second object?

print(type(students[0]))
print(type(students[1]))
# is it the same as the list it self?
# no its not the same. list is list and list objects are strings

print(type(students))

# Print the list's last object

print(students[-1])

# Re-define the second item on the list

students[1] = 'nicole'

# Re-define another item on the list and print all the list

students[4] = 'garth'
print(students)

# Add an item to the list then print the list

students.append('farhiya')
print(students)
# Remove an item from the list then print the list

students.remove(students[3])
print(students)
# Add two items to list then print the list

students.append('mel')
students.append('josh')
print(students)


