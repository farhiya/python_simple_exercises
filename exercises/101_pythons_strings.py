# Define the following variables
# name, last_name, age, eye_color, hair_color, good_person=

first_name = "tom"
last_name = "jerry"
eye_coulor = "yellow"
hair_coulor = "brown"
age = "7"
good_person = "sometimes"
# Ask/Prompt user for input and Re-assign the above variables

first_name = input("what is your first name? ")
last_name = input("what is your last name? ")
eye_colour = input("what colour are your eyes? ")
hair_colour = input("what colour is your hair? ")
age = int(input("what is your age? "))
good_person = input("are you a good person? y/n ")

if good_person == 'y':
    good_person = 'you are a good person'
elif good_person == 'n':
    good_person = 'you are not a good person'
elif good_person != 'y' or good_person != 'n':
    good_person = 'you might be a good person'

# # Print them back to the user as conversation
# # Example: 'Hi there Jack! Your age is 36, you have green eye, black hair and your are a good person!"


print(f"Hi there {first_name} {last_name}! Your age is {age}, you have {eye_colour}, {hair_colour} and {good_person}!")

#Extra Question 2 - Year of birth calculation? and responde back.
# print something like: 'You told the machine you are 28 hence you must have been born 1991!.. give or take'

year_of_birth = 2022 - int(age)
print(f'you were born in {year_of_birth} give or take')

# Extra Question 3 - Cast your input so age is stored as numerical type
age = int(input("what is your age? "))
